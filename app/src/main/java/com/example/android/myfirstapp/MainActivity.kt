package com.example.android.myfirstapp

import android.content.Intent
import android.graphics.Color
import android.os.Bundle
import android.support.constraint.ConstraintLayout
import android.support.v7.app.AppCompatActivity
import android.util.Log
import android.view.Gravity
import android.view.View
import android.widget.Toast
import android.widget.ToggleButton
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        val background: ConstraintLayout = findViewById(R.id.layaoutbackgraund)
        val toggle: ToggleButton = findViewById(R.id.toggleButton)
        toggle.setOnCheckedChangeListener { _, isChecked ->
            if (isChecked){
                background.setBackgroundColor(Color.parseColor("#000000"));
            }else{
                background.setBackgroundColor(Color.parseColor("#F44336"));
            }

        }
    }


    fun toastMe(view: View){
        val toast = Toast.makeText(view.context, "Hello, Toast!", Toast.LENGTH_LONG)
        toast.setGravity(Gravity.CENTER,0,0)
        toast.show()
        Log.d("Tag", "Показать ${toast.toString()}")
    }

    fun countMe(view: View){
        val countString = textView.text.toString()

        var count: Int = Integer.parseInt(countString)
        count++

        textView.text = count.toString()
    }

    fun randomMe(view: View) {
        val randomIntent = Intent(this, SecondActivity::class.java)

        val countString = textView.text.toString()

        val count = Integer.parseInt(countString)

        randomIntent.putExtra(SecondActivity.TOTAL_COUNT, count)

        startActivity(randomIntent)
    }

    fun storageAct(view: View){
        val StorageActivity = Intent(this, StorageActivity::class.java)
        startActivity(StorageActivity)
    }
}
