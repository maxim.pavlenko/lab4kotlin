package com.example.android.myfirstapp;

import android.content.Intent
import android.net.Uri
import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.widget.Switch
import android.widget.Toast
import kotlinx.android.synthetic.main.activity_storage.*
import java.util.*

import kotlin.Suppress;


@Suppress("DEPRECATION")
class StorageActivity : AppCompatActivity() {
    lateinit var locale: Locale
    private var currentLanguage = "en"
    private var currentLang: String? = null



    private fun sendEmail(recipient: String, subject: String, message: String) {

        val mIntent = Intent(Intent.ACTION_SEND)

        mIntent.data = Uri.parse("mailto:")
        mIntent.type = "text/plain"

        mIntent.putExtra(Intent.EXTRA_EMAIL, arrayOf(recipient))
        mIntent.putExtra(Intent.EXTRA_SUBJECT, subject)
        mIntent.putExtra(Intent.EXTRA_TEXT, message)

        try {
            startActivity(Intent.createChooser(mIntent, "Choose Email Client..."))
        } catch (e: Exception) {
            Toast.makeText(this, e.message, Toast.LENGTH_LONG).show()
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_storage)
        currentLanguage = intent.getStringExtra(currentLang).toString()

        val switchLanguage : Switch = findViewById(R.id.language);
        switchLanguage.setOnCheckedChangeListener { _, isChecked ->
            if (isChecked) {
                setLocale("ru")
            } else {
                setLocale("en")
            }
        }
        if(currentLanguage == "ru")  switchLanguage.isChecked=true else switchLanguage.isChecked=false
        send_button.setOnClickListener {
            val recipient = editTextTextEmailAddress.text.toString().trim()
            val last_name = lastName_editText.text.toString().trim()
            val first_name = firstName_editText.text.toString().trim()
            val tex = Nalogy_edit.text.toString().trim()

            val message = "Hi. my name: $first_name. Tex title: $tex"
            val subject = "To $first_name $last_name"

            sendEmail(recipient, subject, message)
        }
    }

    private fun setLocale(localeName: String) {
        if (localeName != currentLanguage) {
            locale = Locale(localeName)
            val res = resources
            val dm = res.displayMetrics
            val conf = res.configuration
            conf.locale = locale
            res.updateConfiguration(conf, dm)
            val refresh = Intent(
                    this,
                    StorageActivity::class.java
            )
            refresh.putExtra(currentLang, localeName)
            startActivity(refresh)
        } else {
            Toast.makeText(
                    this@StorageActivity, "Language, , already, , selected)!", Toast.LENGTH_SHORT).show();
        }
    }


}
